﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlockSchemeApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 1;
            int b = -70;
            int c = 600;

            int D = (b * b) - (4 * a * c);
            double x1 = ((-1 * b) + Math.Sqrt(D)) / 2 * a;
            double x2 = ((-1 * b) - Math.Sqrt(D)) / 2 * a;

            double sum = x1 + x2;

            Console.WriteLine($"Квадратный корень №1 равен {x1}");
            Console.WriteLine($"Квадратный корень №2 равен {x2}");
            Console.WriteLine($"Сумма корней равна {sum}");

            Console.ReadKey();
        }
    }
}
